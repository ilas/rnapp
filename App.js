/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import MasadTinder from "./src/MasadTinder"
import allReducers from './src/redux/reducers/index.js';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from "redux-thunk";

const store = createStore(allReducers, applyMiddleware(thunk));


export default class App extends Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <MasadTinder/>
            </Provider>
        );
    }
}


