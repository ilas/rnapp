import React, {Component} from "react";
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Swipeable from 'react-native-swipeable';
import {connect} from 'react-redux'
import Candidate from "./Candidate"
import Filter from './Filter'
import {
    reject_candidate,
    fetchCandidates,
    fetchTechnologies,
    accept_candidate,
    fetchFilter,
    set_filter,
    filter_candidates
} from "./redux/actions";

import DataStorage from './helper/DataStorage'

class MasadTinder extends React.Component {

    state = {
        currentlyOpenSwipeable: null,

    };

    componentDidMount() {
        this.props.dispatch(fetchFilter())
        this.props.dispatch(fetchCandidates())
        this.props.dispatch(fetchTechnologies())
    }

    handleScroll = () => {
        const {currentlyOpenSwipeable} = this.state;

        if (currentlyOpenSwipeable) {
            currentlyOpenSwipeable.recenter();
        }
    };

    handlerFilterChange(data) {
        const {range, technologies} = data
        this.props.dispatch(set_filter({range: range, picked: technologies}))
        this.props.dispatch(filter_candidates())


    }

    render() {
        const {currentlyOpenSwipeable} = this.state;



        const items = this.props.candidates.map((item, key) => {

            const itemProps = {
                onOpenRight: (event, gestureState, swipeable) => {
                    this.props.dispatch(accept_candidate({candidates: this.props.candidates, id: item._id}))
                },
                onOpenLeft: (event, gestureState, swipeable) => {

                    this.props.dispatch(reject_candidate({candidates: this.props.candidates, id: item._id}))
                },
                onClose: () => this.setState({currentlyOpenSwipeable: null}),
                count: this.props.count,
                serch: this.props.filter.picked

            };


            return <Candidate cd={item} {...itemProps} key={key}/>;
        });


        return (
            <View style={{flex: 1 }}>
            <View style={{height: 100 }}>
                <Filter onChangeRange={this.handlerFilterChange.bind(this)} multiSliderValue={this.props.filter.range}
                        picked={this.props.filter.picked} technologies={this.props.technologies}/>
            </View>

            <ScrollView onScroll={this.handleScroll} style={styles.container}>
                {items}
            </ScrollView>
    </View>

    )
        ;
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    listItem: {
        height: 75,
        alignItems: 'center',
        justifyContent: 'center'
    },
    leftSwipeItem: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20
    },
    rightSwipeItem: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20
    },

});


const mapStateToProps = (state) => {

    const {filter, candidates, technologies} = state
    return {filter, candidates, technologies}
};


export default connect(mapStateToProps)(MasadTinder)
