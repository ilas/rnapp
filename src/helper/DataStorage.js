import {AsyncStorage} from 'react-native';

const FILTER='FILTER_KEY';
const ACCEPTED='ACCEPTED_KEY';
const REJECTED='REJECTED_KEY';

class DataStorage {

    constructor(){
        this.filter = {}
        this._retrieveFilter( FILTER )
        this.candidates = []
    }

    _retrieveFilter = async (key) => {
        try {
            const value = await AsyncStorage.getItem(key);
            if (value !== null) {
                const {range, picked} = JSON.parse(value)

                this.filter = {range, picked}
            }
        } catch (error) {

        }
    }

    async accept( id ){
        this.exclude([id])

        const value = await AsyncStorage.getItem(ACCEPTED);
        let accepted = JSON.parse(value)||[]
        accepted.push(id)
        AsyncStorage.setItem(ACCEPTED, JSON.stringify(accepted));

    }

    async getFromStorage( key ){
        const value = await AsyncStorage.getItem(key);
        let result  = JSON.parse(value)|| []
        return result

    }

    async reject( id ){

        this.exclude([id])

        const value = await AsyncStorage.getItem(REJECTED);
        let rejected = JSON.parse(value)|| []
        rejected.push(id)
        AsyncStorage.setItem(REJECTED, JSON.stringify(rejected));


    }

    exclude(list){

        const d = [].concat(this.candidates)

        this.candidates = d.filter((i)=>(list.indexOf(i._id)<0))

    }


    saveFilter(data){

        this.filter = data

        this._storeData(FILTER, data)

    }


    _storeData = async (key, data) => {
        try {
            await AsyncStorage.setItem(key, JSON.stringify(data));
        } catch (error) {


            // Error saving data
        }
    };



}







export default new DataStorage()
