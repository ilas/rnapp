import DataStorage from "../../helper/DataStorage";


export function filter(data){
    return{
        type: "filterCandidates",
        candidates: data
    };
}


export function filter_candidates(){

    return{
        type: "filterCandidates",
        candidates: DataStorage.candidates,
        filter: DataStorage.filter
    };
}

export function fetch_technologies(data){
    return{
        type: "fetchTechnologies",
        data: data
    };
}
export function fetch_filter(data){
    return{
        type: "fetchFilter",
        data: data
    };
}

export function set_filter(data){
    DataStorage.saveFilter(data)
    return{
        type: "setFilter",
        data: data
    };
}


export function accept_candidate(data){
    DataStorage.accept(data.id)
    return{
        type: "acceptCandidate",
        data: data
    };
}

export function reject_candidate(data){
    DataStorage.reject(data.id)
    return{
        type: "rejectCandidate",
        data: data
    };
}

export function fetchCandidates() {
    return async function(dispatch) {

        let response = await fetch('https://welcome.dropboy.io/candidates')

        let candidates = await response.json()

        let accepted = await DataStorage.getFromStorage('ACCEPTED_KEY')
        let rejected = await DataStorage.getFromStorage('REJECTED_KEY')

        DataStorage.candidates = candidates.slice(1, 50)

        DataStorage.exclude(rejected.concat(accepted) )

        dispatch(filter_candidates());

    };
}

export function fetchTechnologies() {
    return async function(dispatch) {

        let response = await fetch('https://welcome.dropboy.io/technologies')
        let candidates = await response.json()
        dispatch(fetch_technologies(candidates.sort()));
    };
}

export function fetchFilter() {
    return async function(dispatch) {

        let response = await DataStorage._retrieveFilter('FILTER')


        dispatch(fetch_filter(DataStorage.filter));

    };
}
