import {combineReducers} from 'redux';
import candidatesReducer from './candidatesReducer';
import technologiesReducer from './technologiesReducer';
import filterReducer from './filterReducer';
const allReducers= combineReducers({
    candidates: candidatesReducer,
    technologies: technologiesReducer,
    filter: filterReducer
});
export default allReducers;
