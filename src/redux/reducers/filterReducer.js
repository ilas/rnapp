let filter = { range: [0, 20], picked: 'Select technologies'};
export default function(state=[], action){

     switch (action.type) {
         case "setFilter":
         case "fetchFilter":

             if(!action.data.picked)
                 action.data.picked = 'Select technologies'
             if(!action.data.range)
                 action.data.range = [0, 20]

             filter = action.data
             break;

     }

     return filter

}



