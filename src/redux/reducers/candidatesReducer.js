let candidates = [];
export default function(state=[], action){

     switch (action.type) {
         case "rejectCandidate":
         case 'acceptCandidate':
             candidates =  [].concat(state.filter((ite)=>{
                return ite._id!==action.data.id
             }));
             break;
         case "filterCandidates":

             const filter = action.filter

             candidates =  [].concat(action.candidates.filter((ite)=>{

                 for(let  k=0; k<ite.technologies.length; k++){

                     let t  = ite.technologies[k]

                     if((filter.picked=='any' || t.name == filter.picked) && ( t.experianceYears>=filter.range[0] && t.experianceYears<=filter.range[1])){
                         return true
                 }

                 }

                 return false;

             }));
     }
     return candidates

}



