import React, {Component} from "react";
import {StyleSheet, Text, View,  Button} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider'

import ModalFilterPicker from 'react-native-modal-filter-picker'

class Filter extends React.Component {

    state = {
        visible: false,
        multiSliderValue:[1, 10]

    };

    componentDidMount() {

    }


    onShow = () => {
        this.setState({visible: true});
    }

    onSelect = (picked) => {
        this.props.onChangeRange({range:this.props.multiSliderValue, technologies:picked})
        this.setState({
            picked: picked,
            visible: false,

        })
    }

    onCancel = () => {
        this.setState({
            visible: false
        });
    }

    onChangeRange = (data ) => {


        this.setState({
            multiSliderValue: data
        });
        this.setTimer({range:data, technologies:this.props.picked})
    }

    setTimer( data ){

        clearTimeout(this.timer)

        this.timer =  setTimeout(()=>this.props.onChangeRange(data), 1000)

    }


    render() {

        const {visible, picked} = this.state;

        const options = [{key: 'any',label: 'Any'}].concat(this.props.technologies.map((item, key) => {
            return {
                key: item.name,
                label: item.name,
            }
        }))


        return (
            <View  style={{flex: 1,paddingLeft:10, paddingRight:10, alignItems: 'center'}}>
                <View style={{alignItems: 'center'}}>

                    <Text> Experience from {this.props.multiSliderValue[0]} to {this.props.multiSliderValue[1]} </Text>
                </View>

                    <MultiSlider

                        values={this.props.multiSliderValue}

                        min={0}

                        max={50}
                        step={1}
                        allowOverlap
                        snapped

                        onValuesChange={this.onChangeRange.bind(this)}

                    />



                    <View style={{flex: 1, justifyContent: 'flex-start'}}>
                        <Button title={this.props.picked} onPress={this.onShow}/>
                        <ModalFilterPicker
                            visible={visible}
                            onSelect={this.onSelect}
                            onCancel={this.onCancel}
                            options={options}
                        />
                    </View>



            </View>
        )

    }

}


export default Filter

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    listItem: {
        height: 75,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    leftSwipeItem: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20
    },
    rightSwipeItem: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20
    },

});

