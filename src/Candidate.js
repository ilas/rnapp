import React,{Component} from "react";
import Swipeable from 'react-native-swipeable'
import {StyleSheet, Text, View, Image} from 'react-native';

export default function ({cd,onOpenRight, onOpenLeft, onClose, serch}) {
    return (
        <Swipeable

            style={{flex:1}}
           // leftButtonWidth={45}
            leftContent={[
                <View style={[styles.rightSwipeItem, {backgroundColor: 'green'}]}>
                    <Text>Reject</Text>
                </View>,

            ]}
            rightContent={(
                <View style={[styles.rightSwipeItem, {backgroundColor: 'red'}]}>
                    <Text>Accept</Text>
                </View>
            )}
            onRightActionRelease={onOpenRight}
            onLeftActionRelease={onOpenLeft}

        >
            <View style={[styles.listItem, {backgroundColor: ''}]}>
                <View style={{flex:0, height:1, width:'100%', backgroundColor:'#ccc'}}/>
                <View style={{flex:1}}>
                <View style={{flex:1, flexDirection:'row', paddingBottom:5}}>

                    <Image style={{width: 50, height: 50}} source={{uri: cd.picture}}  />
                    <View>
                    <Text  style={{color:'black'}}>{cd.name.first} {cd.name.last} </Text>
                        <Text  style={{color:'black'}}> Age {cd.age} </Text>
                    </View>
                </View>
                    {cd.technologies.map((item, k)=> {

                        const color= (serch == item.name)? {color : 'black'}:{}

                        return   <View style={{flex: 1, textColor: color, paddingLeft:5}}>
                            <Text style={color}   key={k}>{item.name} ({item.experianceYears} year)</Text></View>

                    })}
                </View>
            </View>
        </Swipeable>
    );
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    listItem: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight:10,
        paddingBottom:10,
        flex:1
    },
    leftSwipeItem: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20
    },
    rightSwipeItem: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingLeft: 20
    },

});

